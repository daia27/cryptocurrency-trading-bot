import plotly
import glob
import json
from plotly.graph_objs import Scatter, Layout
from lib.classifiers.classifier import Classifier
from lib.classifiers.strategy import Strategy
from simulation.exchange import Exchange
from simulation.bot import Bot
from os import path

def draw_chart(bot):
    # output to static HTML file
    # output_file("simulation/output.html")

    # Create a new plot with a title and axis labels
    # plot = figure(title="Simulation", x_axis_label='time', y_axis_label='price', width=1500, height=500)

    series_options = {
        'ask': {
            'values_type': 'processed',
            'color': ['#26765C', '#B16B3A'],
            'line_width': 2,
        },
        'bid': {
            'values_type': 'processed',
            'color': ['#80A536', '#9D3355'],
            'line_width': 2,
        },
        'median': {
            'values_type': 'unprocessed',
            'color': ['#00ff00', '#ff0000'],
            'line_width': 3,
        },
        'avg-ask': {
            'values_type': 'processed',
            'color': ['#2E8E2E', '#B13A3A'],
            'line_width': 2,
        },
        'avg-bid': {
            'values_type': 'processed',
            'color': ['#80A536', '#9D3355'],
            'line_width': 2,
        },
        'avg': {
            'values_type': 'processed',
            'color': ['#2E8E2E', '#B13A3A'],
            'line_width': 3,
        },
    }

    plot_data = []
    for series_type in ['avg-ask', 'avg-bid']:
        current_index = 0

        data = {
            bot.classifier.DOWNTREND: {
                'values': [],
                'indexes': [],
            },
            bot.classifier.UPTREND: {
                'values': [],
                'indexes': [],
            },
        }

        for sequence in bot.classifier.series[series_type].sequences:
            if sequence.state() == sequence.UPTREND:
                state = sequence.UPTREND
                counter_state = sequence.DOWNTREND
            else:
                state = sequence.DOWNTREND
                counter_state = sequence.UPTREND

            values = getattr(sequence, series_options[series_type]['values_type'])
            data_fill = [None for _ in values]
            data[state]['values'] += values
            data[state]['indexes'] += sequence.timestamps
            data[counter_state]['values'] += data_fill
            data[counter_state]['indexes'] += data_fill

        for data_type in [bot.classifier.DOWNTREND, bot.classifier.UPTREND]:
            if data_type == bot.classifier.UPTREND:
                processed_color = series_options[series_type]['color'][0]
            else:
                processed_color = series_options[series_type]['color'][1]

            plot_data.append(Scatter(x=data[data_type]['indexes'], y=data[data_type]['values'], name=series_type,
                mode='lines', line=dict(color=processed_color, width=series_options[series_type]['line_width'])))

    def filter_orders(orders, order_type):
        return [o for o in orders if o['side'] == order_type]

    plot_data.append(Scatter(x=[o['timestamp'] for o in filter_orders(exchange.orders, 'BUY')], y=[o['price'] for o in filter_orders(exchange.orders, 'BUY')], name='Buy', mode='markers', line=dict(color='#00ff00')))
    plot_data.append(Scatter(x=[o['timestamp'] for o in filter_orders(exchange.orders, 'SELL')], y=[o['price'] for o in filter_orders(exchange.orders, 'SELL')], name='Sell', mode='markers', line=dict(color='#ff0000')))
    plot_data.append(Scatter(x=[o['timestamp'] for o in exchange.orders], y=[o['price'] for o in exchange.orders],  name='', mode='lines', line=dict(color='#000000', dash='dash', width=0.5)))

    plotly.offline.plot({
        "data": plot_data,
        "layout": Layout(title="Simulation"),
    }, filename='output.html')


if __name__ == '__main__':
    """
    Main entry point of the app
    """
    data = 'WTCBTC'
    timestamp = '1516294466'

    sequence_configuration = {
        'alpha': 0.1,
        'peak_threshold': 0,
        'peak_growth_threshold': 0,
    }

    strategy_configuration = {
        'minimum_current_fluctuation': 0,
        'minimum_previous_fluctuation': 0,
        'stop_loss_threshold': -1
    }

    order_books = []
    symbol_info = None
    data_path = path.join(path.dirname(path.realpath(__file__)), 'data', 'exchange')
    order_books_files = glob.glob(path.abspath(path.join(data_path, '{data}/{timestamp}/order_books.json*'.format(
        data=data, timestamp=timestamp))))
    for order_books_file_path in sorted(order_books_files):
        with open(order_books_file_path) as order_books_file:
            order_books += json.load(order_books_file)[:1000]
    with open(path.join(data_path, '{data}/symbol_info.json'.format(data=data))) as symbol_info_file:
        symbol_info = json.load(symbol_info_file)

    exchange = Exchange(api_key='KEY', api_secret='SECRET', order_books=order_books, symbol_info=symbol_info)
    strategy = Strategy(**strategy_configuration)
    classifier = Classifier(sequence_configuration=sequence_configuration, strategy=strategy)
    bot = Bot(exchange=exchange, classifier=classifier, base='BTC', market='MKT', limit=1000, interval=0,
              on_stop=draw_chart)

    bot.start()

