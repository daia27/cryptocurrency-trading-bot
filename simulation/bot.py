from lib.bot import Bot as BotBase


class Bot(BotBase):
    def __init__(self, exchange, classifier, base, market, limit=1, interval=0, on_start=None, on_stop=None,):
        super().__init__(exchange=exchange, classifier=classifier, base=base, market=market, limit=limit,
                         interval=interval, on_start=on_start, on_stop=on_stop, order_interval=0)

        exchange.set_bot(self)
        self.balance_over_time = []

    def _initialize_balances(self):
        self.balance = {
            'base': {
                'type': 'base',
                'price': float(self.exchange.order_books[0]['bids'][0][0]),
                'quantity': 1,
            },
            'market': {
                'type': 'market',
                'price': float(self.exchange.order_books[0]['asks'][0][0]),
                'quantity': 0,
            },
        }
        self._update_balance_over_time()

    def _initialize_logs(self):
        pass

    def _log_order_book(self, order_book):
        pass

    def update_balances(self, action, quantity, price):
        super().update_balances(action, quantity, price)
        self._update_balance_over_time()

    def _update_balance_over_time(self):
        self.balance_over_time.append(self.balance['base']['quantity'] +
                                      self.balance['market']['quantity'] * self.balance['market']['price'])