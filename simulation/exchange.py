from lib.exchange.binance import Exchange as BaseExchange
from lib.logger import *

class Exchange(BaseExchange):

    def __init__(self, api_key, api_secret, order_books, symbol_info):
        super().__init__(api_key, api_secret)
        self.orders = []
        self.bot = None
        self.current_order_book = 0
        self.order_books = order_books
        self.symbol_info = symbol_info

    def create_order(self, **kwargs):
        """
        Create a fake order with the given arguments

        :param kwargs:
        :return:
        """
        self.orders.append({
            'orderId': len(self.orders),
            'symbol': kwargs['symbol'], #symbol = market + base
            'type': kwargs['type'], #limit order/market order
            'side': kwargs['side'], #sell/buy
            'timeInForce': kwargs['timeInForce'], #how long order stays active
            'origQty': kwargs['quantity'], #targeted quantity
            'price': kwargs['price'], #price measured in base curency
            'executedQty': 0, #executed quantity
        })

        return self.orders[-1]

    def cancel_order(self, **kwargs):
        order = self.orders[kwargs['orderId']]
        order['status'] = self.ORDER_STATUS_CANCELED
        return order

    def get_order(self, **kwargs):
        """
        Return the order with the given id

        :param kwargs:
        :return:
        """
        order = self.orders[kwargs['orderId']]
        order['executedQty'] = order['origQty']
        order['status'] = self.ORDER_STATUS_FILLED
        return order

    def get_historical_trades(self, **kwargs):
        return []

    def get_order_book(self, **kwargs):
        """
        Get an order book from the order books file

        :param symbol:
        :return:
        """
        try:
            order_book = self.order_books[self.current_order_book]
        except IndexError:
            logger.balances(balance=self.bot.balance, currency=self.bot.currency)
            logger.balances_total(balance=self.bot.balance, currency=self.bot.currency)

            return self.bot.stop()

        self.current_order_book += 1
        return order_book

    def get_ticker(self, **kwargs):
        #Ask and Bid prices at current moment

        order_book = self.get_order_book(**kwargs)

        return {
            'askPrice': order_book['asks'][0][0],
            'bidPrice': order_book['bids'][0][0],
        }

    def get_symbol_ticker(self, **kwargs):
        #Returns last transaction average price (asks+bids)/2

        return {
            'price': (self.order_books[self.current_order_book]['asks'][0][0] +
                      self.order_books[self.current_order_book]['bids'][0][0])/2
        }

    def get_symbol_info(self, **kwargs):
        return self.symbol_info

    def get_asset_balance(self, **kwargs):
        if kwargs['asset'] == 'BTC':
            return {
                'free': 1
            }

        return {
            'free': 0
        }

    def ping(self):
        return True

    def set_bot(self, bot):
        """
        Set the bot to be used for internal triggers

        :param bot:
        :return:
        """
        self.bot = bot
