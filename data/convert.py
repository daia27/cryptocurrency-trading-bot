from os import path, makedirs
import glob
import json


for input_path in glob.glob('raw/*/*.json'):
    print('Reading %s' % input_path)
    with open(input_path, 'r') as input_file:
        data = input_file.read()
        data = '[' + data[:-1] + ']'
        data = json.loads(data)

    order_books = []
    for order_book in data:
        order_books.append({
            'asks': list(map(lambda order: [order['price'], order['quantity'], []], order_book['asks'])),
            'bids': list(map(lambda order: [order['price'], order['quantity'], []], order_book['bids'])),
        })

    output_folder = 'exchange/{basename}/{timestamp}'.format(
        basename=path.basename(path.splitext(input_path)[0]), timestamp=input_path.split('/')[1])
    if not path.isdir(output_folder):
        makedirs(output_folder)
    output_path = path.abspath(path.join(output_folder, 'order_books.json'))

    print('Writing %s' % output_path)
    with open(output_path, 'w+') as output_file:
        json.dump(order_books, output_file)

