from os import path
import glob
import json
from binance.client import Client
from time import sleep

client = Client('', '')


for input_path in glob.glob('exchange/*'):
    symbol = input_path.split('/')[-1]
    output_path = path.join(input_path, 'symbol_info.json')

    print('Getting symbol info for %s' % symbol)

    info = client.get_symbol_info(symbol='BNBBTC')

    print('Writing %s' % output_path)
    with open(output_path, 'w+') as output_file:
        json.dump(info, output_file)

    sleep(1)

