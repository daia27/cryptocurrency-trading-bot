import pprint
from time import sleep
from lib.logger import logger
from lib.exchange.binance import APIException


class Order(object):
    def __init__(self, symbol, balance, info, exchange, on_cancel=None, on_complete=None, interval=1, timestamp=None):
        self.symbol = symbol
        self.balance = balance
        self.info = info
        self.exchange = exchange
        self.order_id = None
        self.complete = False
        self.interval = interval
        self.timestamp = timestamp

        self.on_cancel = on_cancel
        self.on_complete = on_complete
        pprint.pprint(info)


    def _complete(self, exchange_order):
        """
        When an order is completed or cancelled, stop the order verification loop and update the balance

        :param exchange_order:
        :return:
        """
        if self.on_complete:
            self.on_complete(order=exchange_order, info=self.info)
        self.complete = True

    def _cancel(self, exchange_order, request=True):
        """
        When an order is cancelled, complete the current order execution and run the bot's ticker to replace the order

        :param exchange_order:
        :return:
        """
        if request:
            self.exchange.cancel_order(symbol=self.symbol, orderId=self.order_id)

        if self.on_cancel:
            self.on_cancel(order=exchange_order, info=self.info)
        self.complete = True


    def _place_order(self):
        """
        Place an order using the current exchange

        :return:
        """
        try:
            order = self.exchange.create_order(
                symbol=self.symbol,
                side=self.info['action'].upper(),
                type=self.exchange.ORDER_TYPE_LIMIT,
                timeInForce=self.exchange.TIME_IN_FORCE_GTC,
                quantity=self.info['quantity'],
                price=self.info['price'])

            # only for graphical representation

            if self.exchange.orders:
                self.exchange.orders[-1]['timestamp'] = self.timestamp

            return order
        except APIException as exception:
            logger.error(exception)

            sleep(self.interval)
            return self._place_order()

    def _verify(self):
        """
        Verify the status of the placed order

        :return:
        """
        try:
            order = self.exchange.get_order(symbol=self.symbol, orderId=self.order_id)

            # pprint.pprint('verify')
            # pprint.pprint(order)

            # If the order is filled, all we need to do is to update the balance.
            if order['status'] == self.exchange.ORDER_STATUS_FILLED:
                self._complete(exchange_order=order)

            # If the order doesn't exist anymore, update the balance in case it was partially filled
            # and continue on next ticker
            elif order['status'] in [self.exchange.ORDER_STATUS_REJECTED, self.exchange.ORDER_STATUS_EXPIRED]:
                self._complete(exchange_order=order)

            elif order['status'] in [self.exchange.ORDER_STATUS_CANCELED, self.exchange.ORDER_STATUS_PENDING_CANCEL]:
                self._cancel(exchange_order=order, request=False)

            else:
                # Get the current ticker to see whether there's an order that has better price than ours
                ticker = self.exchange.get_ticker(symbol=self.symbol)

                # Check whether there's a better price than ours. If there is, replace the order
                if self.info['type'] == 'bid' and float(ticker['bidPrice']) > self.info['price'] or \
                   self.info['type'] == 'ask' and float(ticker['askPrice']) < self.info['price']:
                    self._cancel(exchange_order=order)

        except APIException as exception:
            logger.error(exception)


    def place(self):
        order = self._place_order()

        # pprint.pprint('place')
        # pprint.pprint(order)
        self.order_id = order['orderId']

        while not self.complete:
            sleep(self.interval)
            self._verify()
