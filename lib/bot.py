from time import sleep, time
from lib.logger import logger
from lib.order import Order
from lib.exchange.binance import APIException
import os
import math

class Bot(object):
    """
    A cryptocurrency trading bot that checks the market feasibility periodically and executes automated market orders
    """

    def __init__(self, exchange, classifier, base, market, limit=1, interval=5, order_interval=1, on_start=None,
                 on_stop=None):
        self.exchange = exchange
        self.classifier = classifier

        self.interval = interval # time interval for checking market status
        self.order_interval = order_interval # time interval for checking order status

        self.balance = {} # current balance
        self.balance_limit = float(limit) # controls the amount of base curency used in a transaction

        self.currency = {
            'base': base,
            'market': market
        }

        self.symbol = '{market}{base}'.format(base=base, market=market)
        self.running = None
        self.info = None
        self.history = None
        self.filter = {
            'min_price': 0,
            'max_price': math.inf,
            'min_quantity': 0,
            'max_quantity': math.inf,
            'min_notional': 0
        }
        self.log_files = {}

        self.on_stop = on_stop
        self.on_start = on_start

    def _initialize_symbol_info(self):
        """
        Get information about the current symbol

        :return:
        """
        try:
            self.info = self.exchange.get_symbol_info(symbol=self.symbol)

            self.classifier.strategy.price_precision = self.info['quotePrecision']
            self.classifier.strategy.quantity_precision = self.info['baseAssetPrecision'] - self.info['quotePrecision']

            for order_filter in self.info['filters']:
                if order_filter['filterType'] == self.exchange.FILTER_TYPE_PRICE_FILTER:
                    self.filter['min_price'] = float(order_filter['minPrice'])
                    self.filter['max_price'] = float(order_filter['maxPrice'])
                    self.filter['tick_size'] = abs(math.log10(float(order_filter['tickSize'])))
                    self.classifier.strategy.price_precision = self.filter['tick_size']
                elif order_filter['filterType'] == self.exchange.FILTER_TYPE_LOT_SIZE:
                    self.filter['min_quantity'] = float(order_filter['minQty'])
                    self.filter['max_quantity'] = float(order_filter['maxQty'])
                    self.filter['step_size'] = abs(math.log10(float(order_filter['stepSize'])))
                    self.classifier.strategy.quantity_precision = self.filter['step_size']
                elif order_filter['filterType'] == self.exchange.FILTER_TYPE_MIN_NOTIONAL:
                    self.filter['min_notional'] = float(order_filter['minNotional'])

        except APIException as exception:
            logger.error(exception)

            sleep(1)
            self._initialize_symbol_info()

    def _initialize_history(self):
        """
        Get order history for the current symbol

        :return:
        """
        try:
            self.history = self.exchange.get_historical_trades(symbol=self.symbol)

        except APIException as exception:
            logger.error(exception)

            sleep(1)
            self._initialize_history()

    def _initialize_balances(self):
        """
        Get available balance quantities

        :return:
        """
        try:
            ticker = self.exchange.get_symbol_ticker(symbol=self.symbol)

            for currency_type in ['base', 'market']:
                self.balance[currency_type] = {
                    'type': currency_type,
                    'price': float(ticker.get('price', 1)),
                    'quantity': float(self.exchange.get_asset_balance(asset=self.currency[currency_type]).get('free', 0)),
                }

        except APIException as exception:
            logger.error(exception)

            sleep(1)
            self._initialize_balances()

    def _get_order_book(self):
        """
        Get the current order book from the exchange

        :return:
        """
        # Get the exchange order book entries
        try:
            order_book = self.exchange.get_order_book(symbol=self.symbol)
        except APIException as exception:
            logger.error(exception)
            sleep(self.interval)
            return self._get_order_book()

        return order_book

    def update_balances(self, action, quantity, price):
        """
        Update the balances when an order is complete with the executed order quantity

        :param action:      The action being taken, buy or sell
        :param quantity:    The order quantity in market currency
        :param price:       The order price in base currency
        :return:
        """
        if action == 'buy':
            self.balance['base']['quantity'] -= quantity * price
            self.balance['market']['quantity'] += quantity * (1 - self.exchange.COMMISSION)
            self.balance['market']['price'] = price
        else:
            self.balance['market']['quantity'] -= quantity
            self.balance['base']['quantity'] += quantity * (1 - self.exchange.COMMISSION) * price
            self.balance['base']['price'] = price

    def _validate_balances(self):
        """
        Validate if we have enough base balance to meet the minimum notional quantity requirements

        :return:
        """
        if self.balance['market']['quantity'] * self.balance['market']['price'] + self.balance['base']['quantity'] < self.filter['min_notional']:
            logger.error('Minimum notional value of {notional}{base} not met.'.format(
                notional=self.filter['min_notional'], base=self.currency['base']))
            self.stop()

    def _limit_balances(self):
        """
        Limit the whole balance amount to the given threshold

        :return:
        """
        if self.balance['market']['quantity'] * self.balance['market']['price'] >= self.balance_limit:
            self.balance['base']['quantity'] = 0
            self.balance['market']['quantity'] = self.balance_limit / self.balance['market']['price']

        elif self.balance['base']['quantity'] >= self.balance_limit:
            self.balance['base']['quantity'] = self.balance_limit
            self.balance['market']['quantity'] = 0

        else:
            market_to_base_quantity = self.balance['market']['quantity'] * self.balance['market']['price']
            remaining_quantity = self.balance_limit - market_to_base_quantity

            if self.balance['base']['quantity'] > remaining_quantity:
                self.balance['base']['quantity'] = remaining_quantity

    def _on_order_complete(self, order, info):
        """
        Order complete callback for updating balances with the placed orders executed quantity and price

        :param order:
        :param info:
        :return:
        """
        self.update_balances(info['action'], float(order['executedQty']), float(order['price']))

    def _on_order_cancel(self, order, info):
        """
        Order cancel callback for updating balances with the placed orders executed quantity and price

        :param order:
        :param info:
        :return:
        """
        self.update_balances(info['action'], float(order['executedQty']), float(order['price']))
        self.classifier.reuse_sequence(info['analysis_type'], info['action'])
        self.tick()

    def _initialize_logs(self):
        """
        Initialize log folders and files

        :return:
        """
        self.log_files['dir'] = directory = os.path.abspath('data/exchange/{exchange}/{base}-{market}'.format(
            exchange=self.exchange.name.lower(), base=self.currency['base'], market=self.currency['market']))
        order_books_directory = os.path.join(directory, str(int(time())))

        if not os.path.exists(directory):
            os.makedirs(directory)
        os.makedirs(order_books_directory)

        self.log_files['symbol_info'] = logger.log_to_file(os.path.join(directory, 'symbol_info.json'), self.info)
        self.log_files['historical_trades'] = logger.log_to_file(os.path.join(directory, 'historical_trades.json'), self.history)
        self.log_files['order_books'] = logger.log_to_file(os.path.join(order_books_directory, 'order_books.json'), [])

    def _log_order_book(self, order_book):
        self.log_files['order_books'] = logger.log_to_file(self.log_files['order_books'], order_book, True)

    def tick(self):
        """
        Verify current market status

        :return:
        """
        order_book = self._get_order_book()
        timestamp = time()

        self._log_order_book(order_book)

        if not order_book:
            return

        # Analyze the current order book entry
        analysis = self.classifier.resolve(order_book=order_book, balance=self.balance, timestamp=timestamp)

        for currency_type in ['base', 'market']:
            feasibility = analysis[currency_type]

            if feasibility['action'] == 'hold':
                continue

            order = Order(
                info=feasibility,
                symbol=self.symbol,
                balance=self.balance,
                interval=self.order_interval,
                exchange=self.exchange,
                on_cancel=self._on_order_cancel,
                on_complete=self._on_order_complete,
                timestamp=timestamp,)
            order.place()
            logger.balances(balance=self.balance, currency=self.currency)


    def start(self):
        """
        Run the market feasibility check at the given time interval

        :return:
        """
        self.running = True

        self._initialize_symbol_info()
        self._initialize_history()
        self._initialize_balances()


        # Logging
        logger.info('Starting {base}-{market} bot on {exchange}.'.format(base=self.currency.get('base'),
                                                                         market=self.currency.get('market'),
                                                                         exchange=self.exchange.name))

        # Check whether we can start the bot having the given balance
        self._validate_balances()
        self._limit_balances()

        # Log currency balances
        logger.balances(balance=self.balance, currency=self.currency)

        # Pass info and filters to classifier
        self.classifier.info = self.info
        self.classifier.filter = self.filter

        if self.on_start:
            self.on_start(self)

        # Start the ticker loop
        while self.running:
            self.tick()

            # Rerun the ticker after the given time interval has passed
            sleep(self.interval)

    def stop(self):
        """
        Stop the bot execution by cancelling the ticker's while loop

        :return:
        """
        self.running = False

        if self.on_stop:
            self.on_stop(self)
