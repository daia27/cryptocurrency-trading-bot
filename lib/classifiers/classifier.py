from lib.classifiers.series import Series
from lib.classifiers.strategy import Strategy
from lib.logger import logger
import math


class Classifier:
    """
        Analyze balance transaction feasibility using the current order book
        """

    ACTIONS = {
        'base': 'buy',
        'market': 'sell',
    }

    UPTREND = 1
    NONE = 0
    DOWNTREND = -1

    def __init__(self, sequence_configuration, strategy=Strategy()):
        self.strategy = strategy
        self.order_books = []

        # Inherited from bot
        self.info = None
        self.filter = None

        self.series = {
            'ask': Series(sequence_configuration),
            'bid': Series(sequence_configuration),
            'median': Series(sequence_configuration),

            'avg-ask': Series(sequence_configuration),
            'avg-bid': Series(sequence_configuration),
            'avg': Series(sequence_configuration),
        }

    def resolve(self, order_book, balance, timestamp):
        """
        Analyze the current order book entries and resolve a feasibility analysis

        :param order_book:
        :param balance:
        :param timestamp:
        :return:
        """
        average_orders_count = 10

        ask = float(order_book['asks'][0][0])
        bid = float(order_book['bids'][0][0])
        median = (ask + bid) / 2
        average_ask = Classifier.get_average_price_by_count(order_book['asks'], average_orders_count)
        average_bid = Classifier.get_average_price_by_count(order_book['bids'], average_orders_count)
        average = (average_ask['price'] + average_bid['price']) / 2

        self.series['ask'].add(ask, timestamp)
        self.series['avg-ask'].add(average_ask['price'], timestamp)

        self.series['bid'].add(bid, timestamp)
        self.series['avg-bid'].add(average_bid['price'], timestamp)

        self.series['median'].add(median, timestamp)
        self.series['avg'].add(average, timestamp)

        logger.separator()
        logger.sequence(title='Ask', sequence=self.series['avg-ask'].sequences[-1])
        logger.sequence(title='Avg', sequence=self.series['avg'].sequences[-1])
        logger.sequence(title='Bid', sequence=self.series['avg-bid'].sequences[-1])

        analysis = {}
        for currency_type in ['base', 'market']:
            analysis[currency_type] = getattr(self.strategy, self.ACTIONS[currency_type])(self, balance[currency_type])

        return analysis

    def is_trend(self, series_type, state):
        """
        Check whether the last sequence of the given type is in the given trend state

        :param series_type:
        :param state:
        :return:
        """
        return self.series[series_type].sequences[-1].state() == state

    def is_uptrend(self, series_type):
        """
        Check whether the last sequence is in an uptrend state

        :param series_type:
        :return:
        """
        return self.is_trend(series_type, self.UPTREND)

    def is_downtrend(self, series_type):
        """
        Check whether the last sequence is in a downtrend state

        :param series_type:
        :return:
        """
        return self.is_trend(series_type, self.DOWNTREND)

    def use_sequence(self, series_type, action):
        """
        Check whether the current sequence has been already used in the
        feasibility analysis

        :return:
        """
        return self.series[series_type].sequences[-1].use(action)

    def reuse_sequence(self, series_type, action):
        """
        Check whether the current sequence has been already used in the
        feasibility analysis

        :return:
        """
        return self.series[series_type].sequences[-1].reuse(action)

    def is_sequence_used(self, series_type, action):
        """
        Check whether the current sequence has been already used in the
        feasibility analysis

        :return:
        """
        return self.series[series_type].sequences[-1].used.get(action, False)

    def get_profit(self, series_type, balance):
        """
        Get the current profit of a balance

        :param series_type:
        :param balance:
        :return:
        """
        return self.series[series_type].sequences[-1].unprocessed[-1] * 100 / balance['price'] - 100

    def is_minimum_profit(self, series_type, balance, threshold):
        """
        Verify if the profit is greater than the given threshold

        :param series_type:
        :param balance:
        :param threshold:
        :return:
        """
        return self.get_profit(series_type=series_type, balance=balance) > threshold

    def is_stop_loss(self, series_type, balance, threshold):
        """
        Verify if we're entering a stop loss state, where the rate at which we
        purchased the currency is much higher than the current rate

        :param series_type:
        :param balance:
        :param threshold:
        :return:
        """
        return self.get_profit(series_type=series_type, balance=balance) < threshold

    def is_minimum_fluctuation(self, series_type, threshold):
        """
        Verify if the fluctuation of the last sequence is substantial enough

        :param series_type:
        :param threshold:
        :return:
        """
        return abs(self.series[series_type].sequences[-1].fluctuation()) >= threshold

    def was_minimum_fluctuation(self, series_type, threshold):
        """
        Verify if the fluctuation of the one before last sequence is substantial enough

        :param series_type:
        :param threshold:
        :return:
        """
        if len(self.series[series_type].sequences) < 2:
            return False

        return abs(self.series[series_type].sequences[-2].fluctuation()) >= threshold

    def validate_order_quantity(self, quantity):
        """
        Check that the minimum order quantity requirements are met

        :param quantity:
        :return:
        """
        return self.filter['min_quantity'] < quantity < self.filter['max_quantity']

    def validate_order_price(self, price):
        """
        Check that the minimum order price requirements are met

        :param price:
        :return:
        """
        return self.filter['min_price'] < price < self.filter['max_price']

    def validate_order_notional(self, quantity, price):
        """
        Check that the minimum order notional amount requirements are met

        :param quantity:
        :param price:
        :return:
        """
        return quantity * price >= self.filter['min_notional']

    def validate_order(self, quantity, price):
        """
        Check that the minimum order requirements are met

        :param quantity:
        :param price:
        :return:
        """
        return self.validate_order_quantity(quantity) and self.validate_order_price(price) and self.validate_order_notional(quantity, price)


    @staticmethod
    def get_average_price_by_count(orders, count=10):
        """
        Calculate average rate of the first n orders

        :param orders:
        :param count: Get all orders up to threshold base quantity
        :return:
        """

        base_quantity = 0
        market_quantity = 0

        for order in orders[:count]:
            base_quantity += float(order[0]) * float(order[1])
            market_quantity += float(order[1])

        return {
            'price': base_quantity / market_quantity,
            'quantity': market_quantity
        }
