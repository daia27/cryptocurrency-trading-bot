from lib.classifiers.sequence import Sequence


class Series(object):
    def __init__(self, sequence_configuration):
        self.sequences = []
        self.sequence_configuration = sequence_configuration

    def add(self, value, timestamp):
        """
        Add a new value to the last sequence

        :param value:
        :return:
        """

        if not self.sequences:
            self.sequences.append(Sequence(**self.sequence_configuration))

        last_sequence = self.sequences[-1]
        added = last_sequence.add(value, timestamp)

        if added:
            return

        new_sequence = Sequence(**self.sequence_configuration)
        new_sequence.add(last_sequence.processed[-1], last_sequence.timestamps[-1])
        new_sequence.add(value, timestamp)

        self.sequences.append(new_sequence)
