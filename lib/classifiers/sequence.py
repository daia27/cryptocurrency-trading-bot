class Sequence(object):
    """
    A sequence of values following an ascending or descending trend
    """

    UPTREND = 1
    NONE = 0
    DOWNTREND = -1

    def __init__(self, alpha=0.5, peak_threshold=1, peak_growth_threshold=0.1):
        self.processed = [] # normalized data after single exponential smoothing
        self.unprocessed = [] # data to be normalized
        self.timestamps = [] # the time when a value has been acquired from the above arrays
        self.used = {} # flag for marking if a transaction has been made on the current sequence

        self.alpha = alpha # smoothing coeficient
        self.peak_threshold = peak_threshold # error margin at which no transaction is being made
        self.peak_growth_threshold = peak_growth_threshold # tollerance for trend change

    def add(self, value, timestamp):
        """
        Add a new value to the sequence if the given value maintains the current trend, or is close the current peak

        :param value:
        :return:
        """

        unprocessed = value
        processed = self._process(value)

        if self._is_maintaining_trend(processed) or self._is_close_to_peak(processed) or len(self.processed) < 2:
            self.processed.append(processed)
            self.unprocessed.append(unprocessed)
            self.timestamps.append(timestamp)
            return True
        return False

    def _is_maintaining_trend(self, value):
        """
        Check whether the given value maintains the current sequence trend

        :param value:
        :return:
        """
        return (self.state() == self.DOWNTREND and self.processed[-1] >= value) or (self.state() == self.UPTREND and self.processed[-1] <= value)

    def fluctuation_of(self, value):
        """
        Calculate sequence fluctuation from first trend point to the last trend point

        :param value:
        :return:
        """
        if not self.processed:
            return 0

        return value * 100 / self.processed[0] - 100

    def fluctuation(self):
        """
        Calculate sequence fluctuation from first trend point to the last trend point

        :return:
        """
        if not self.processed:
            return 0

        return self.processed[-1] * 100 / self.processed[0] - 100

    def state(self):
        """
        Get the state of the current sequence

        :return:
        """

        state = self.NONE

        if len(self.processed) >= 2:
            first = self.processed[0]
            second = self.processed[-1]

            if first > second:
                state = self.DOWNTREND
            elif first < second:
                state = self.UPTREND

        return state

    def peak(self):
        """
        Calculate the peak value of the current sequence. Returns the minimum value for downtrend sequential and the
        maximum value for uptrend sequential.

        :return:
        """
        if not self.processed:
            return None

        if self.state() == self.DOWNTREND:
            return min(self.processed)
        return max(self.processed)

    def use(self, action):
        """
        Set the current sequence as used for the given action.

        :param action:
        :return:
        """
        self.used[action] = True

    def reuse(self, action):
        """
        Set the current sequence as unused for the given action.

        :param action:
        :return:
        """
        self.used[action] = False

    def _process(self, value):
        """
        Process given value based on alpha normalization and the previous state

        :param value:
        :return:
        """
        if len(self.processed) < 2:
            return value

        return self.alpha * value + (1 - self.alpha) * self.processed[-1] # how much the last value influences the newly add value

    def _is_close_to_peak(self, value):
        if self.state() == self.NONE:
            return False

        peak_fluctuation = abs(self.fluctuation_of(self.peak()))
        value_fluctuation = abs(self.fluctuation_of(value))
        delta_fluctuation = abs(peak_fluctuation - value_fluctuation)

        if value_fluctuation > self.peak_threshold:
            normalized_peak_threshold = self.peak_threshold + (value_fluctuation - self.peak_threshold) * self.peak_growth_threshold
        else:
            normalized_peak_threshold = self.peak_threshold

        return delta_fluctuation < normalized_peak_threshold
