import math

class Strategy(object):
    """
    A trading strategy that analyzes when it's feasible to buy or sell
    """

    def __init__(self, minimum_current_fluctuation=1, minimum_previous_fluctuation=5, stop_loss_threshold=-1):
        self.minimum_current_fluctuation = minimum_current_fluctuation
        self.minimum_previous_fluctuation = minimum_previous_fluctuation
        self.stop_loss_threshold = stop_loss_threshold
        self.price_difference_factor = 1 / 10
        self.price_precision = 8
        self.quantity_precision = 8

    @staticmethod
    def get_initial_feasibility(analysis_type, series_type):
        """
        Return an empty feasibility object

        :param analysis_type:
        :param series_type:
        :return:
        """
        return {
            'action': 'hold',
            'feasible': False,
            'analysis_type': analysis_type, # the type of series to analyse (avg, ask, bid etc)
            'type': series_type, # the actual series type (bid or ask)
        }

    def format_precision(self, value, precision):
        return math.floor(value * 10 ** precision) / 10 ** precision

    def buy(self, classifier, balance):
        """
        Analyze buying feasibility

        :param classifier:
        :param balance:
        :return:
        """
        analysis_type = 'avg-bid'
        series_type = 'bid'
        counter_series_type = 'ask'
        action = 'buy'

        feasibility = Strategy.get_initial_feasibility(analysis_type, series_type)

        is_feasible_fluctuation = classifier.is_minimum_fluctuation(analysis_type, self.minimum_current_fluctuation) or \
            classifier.was_minimum_fluctuation(analysis_type, self.minimum_previous_fluctuation)
        is_feasible_trend = classifier.is_uptrend(analysis_type)
        is_usable_sequence = not classifier.is_sequence_used(analysis_type, action)

        feasibility.update({
            'is_feasible_fluctuation': is_feasible_fluctuation,
            'is_feasible_trend': is_feasible_trend,
            'is_usable_sequence': is_usable_sequence,
        })

        values = classifier.series[series_type].sequences[-1].unprocessed
        counter_values = classifier.series[counter_series_type].sequences[-1].unprocessed
        difference = values[-1] - counter_values[-1] # required to determinate at which price to set the order

        price = values[-1] + abs(difference) * self.price_difference_factor
        quantity = balance['quantity'] / price
        is_valid_order = classifier.validate_order(quantity, price)

        if is_valid_order and is_feasible_trend and is_feasible_fluctuation:
            feasibility['action'] = action
            feasibility['feasible'] = True
            feasibility['price'] = self.format_precision(price, self.price_precision)
            feasibility['quantity'] = self.format_precision(quantity, self.quantity_precision)

            classifier.use_sequence(series_type, action)
            classifier.use_sequence(counter_series_type, action)

        return feasibility

    def sell(self, classifier, balance):
        """
        Analyze selling feasibility

        :param classifier:
        :param balance:
        :return:
        """
        analysis_type = 'avg-ask'
        series_type = 'ask'
        counter_series_type = 'bid'
        action = 'sell'

        feasibility = Strategy.get_initial_feasibility(analysis_type, series_type)

        is_feasible_fluctuation = classifier.is_minimum_fluctuation(analysis_type, self.minimum_current_fluctuation) or \
            classifier.was_minimum_fluctuation(analysis_type, self.minimum_previous_fluctuation)
        is_feasible_trend = classifier.is_downtrend(analysis_type)
        is_usable_sequence = not classifier.is_sequence_used(analysis_type, action)
        is_stop_loss = classifier.is_stop_loss(series_type, balance, self.stop_loss_threshold)

        feasibility.update({
            'is_feasible_fluctuation': is_feasible_fluctuation,
            'is_feasible_trend': is_feasible_trend,
            'is_usable_sequence': is_usable_sequence,
            'is_stop_loss': is_stop_loss
        })

        values = classifier.series[series_type].sequences[-1].unprocessed
        counter_values = classifier.series[counter_series_type].sequences[-1].unprocessed
        difference = values[-1] - counter_values[-1]

        price = values[-1] + abs(difference) * self.price_difference_factor
        # When entering stop loss state, sell instantly
        if is_stop_loss:
            price = counter_values[-1]
        quantity = balance['quantity']

        is_valid_order = classifier.validate_order(quantity, price)

        if is_valid_order and (is_feasible_trend and is_feasible_fluctuation or is_stop_loss):
            feasibility['action'] = action
            feasibility['feasible'] = True
            feasibility['price'] = self.format_precision(price, self.price_precision)
            feasibility['quantity'] = self.format_precision(quantity, self.quantity_precision)

            classifier.use_sequence(series_type, action)
            classifier.use_sequence(counter_series_type, action)

        return feasibility

