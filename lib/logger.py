import re
import json
from logzero import logger


def balances(balance, currency):
    logger.info('Current balance status:')
    print('[{base}] {quantity:.8f}{base} at {price:.8f}{market}'.format(
        base=currency['base'], market=currency['market'],
        quantity=balance['base']['quantity'], price=balance['base']['quantity'] / balance['base']['price']))
    print('[{market}] {quantity:.8f}{market} at {price:.8f}{base}'.format(
        market=currency['market'], base=currency['base'],
        quantity=balance['market']['quantity'], price=balance['market']['price']))


def balances_total(balance, currency):
    logger.info('Total base balance: {balance}{base}'.format(
        balance=balance['base']['quantity'] + balance['market']['quantity'] * balance['base']['price'],
        base=currency['base']
    ))


def sequence(title, sequence):
    fluctuation = '%.2f' % abs(sequence.fluctuation())
    fluctuation_whitespace = '       '[:len(fluctuation) - 7]
    value = '%.8f' % sequence.unprocessed[-1]
    state_icon = '='
    state_sign = '-'
    if sequence.state() == sequence.UPTREND:
        state_icon = u'\u2197'
        state_sign = '+'
    elif sequence.state() == sequence.DOWNTREND:
        state_icon = u'\u2198'
        state_sign = '-'

    print(u'{title}  {whitespace}{sign}{fluctuation}%  |  {value}'.format(
        title=title, whitespace=fluctuation_whitespace, sign=state_sign, fluctuation=fluctuation, value=value))


def separator():
    print('=' * 80)


def log_to_file(file_path, data, append=False):
    if append:
        with open(file_path, 'r+') as file:
            file_data = json.load(file)
            file.close()

        if len(file_data) == 1000:
            match = re.search('.*\\.([0-9]+)$', file_path)

            if match:
                file_path = re.sub(r'' + match.group(1) + '$', str(int(match.group(1)) + 1), file_path)
            else:
                file_path = file_path + '.1'

            return log_to_file(file_path, [data])

        file_data.append(data)
        with open(file_path, 'w+') as file:
            json.dump(file_data, file)
            file.close()

    else:
        with open(file_path, 'w+') as file:
            json.dump(data, file)
            file.close()

    return file_path


logger.separator = separator
logger.balances = balances
logger.balances_total = balances_total
logger.log_to_file = log_to_file
logger.sequence = sequence
